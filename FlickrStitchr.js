var stitchPaneIsHidden = true;
function showStitch() {
  if(stitchPaneIsHidden == true) {
    $("#stitchPane").show();
    stitchPaneIsHidden = false;
  }
  else {
    $("#stitchPane").hide();
    stitchPaneIsHidden = true;
  }
}

var imagesToStitch = new Array();
function drop(ev)
{
  ev.preventDefault();
  var data=ev.dataTransfer.getData("srcURL");
  var img = document.createElement("img");
  img.setAttribute("src", data);
  img.setAttribute("width", "75px");
  ev.target.appendChild(img);
  imagesToStitch.push(data);
}

function drag(ev) {
        ev.dataTransfer.setData("srcURL",$(ev.target).parent().children("img").attr("src"));
}

function doStitch(filenames) {
   var str = JSON.stringify(filenames);

   $.ajax({
      url: "doStitch.php",
      type: "GET",
      data: "image_files="+str,
      contentType: "application/json",
      success: function(imgData)
      {
         imgData = imgData.split("<!--");
         console.log(imgData);
         document.write(imgData[0]);
      },
      error: function(xhr, textStatus, errorThrown)
      {
         alert("An error occured");
         console.log(errorThrown);
         console.log(textStatus);
         console.log(xhr);
      }
   });
}

function formatImages() {
  $(".play").each(function() {
	  console.log("hi");

	$(this).attr("draggable", "true");
        $(this).attr("ondragstart", "drag(event)");
  });
}
